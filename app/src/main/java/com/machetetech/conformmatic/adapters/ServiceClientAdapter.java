package com.machetetech.conformmatic.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.machetetech.conformmatic.R;
import com.machetetech.conformmatic.models.ServiceClient;
import com.machetetech.conformmatic.models.ServiceItem;
import com.machetetech.conformmatic.utils.ViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rolangom on 11/7/14.
 */
public class ServiceClientAdapter extends BaseAdapter {

    final List<ServiceClient> mList;
    final LayoutInflater mInflater;

    public ServiceClientAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        mList = new ArrayList<ServiceClient>();
    }

    public void addRange(List<ServiceClient> newRange) {
        mList.addAll(newRange);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public ServiceClient getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null)
            view = mInflater.inflate(R.layout.item_list_service, viewGroup, false);

        View lineView = ViewHolder.get(view, R.id.vline);
        TextView titleView = ViewHolder.get(view, R.id.title);
        TextView addressView = ViewHolder.get(view, R.id.descr);

        ServiceClient item = getItem(i);

        lineView.setBackgroundResource(item.getColorRes());
        titleView.setTextColor(view.getResources().getColor(item.getColorRes()));
        titleView.setText(item.servType);
        addressView.setText(item.address);

        titleView.setCompoundDrawablesWithIntrinsicBounds
                (0, 0, item.isPostponed() ? android.R.drawable.ic_menu_recent_history : 0, 0);

        return view;
    }
}
