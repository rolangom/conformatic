package com.machetetech.conformmatic.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.machetetech.conformmatic.R;
import com.machetetech.conformmatic.models.ServiceItem;
import com.machetetech.conformmatic.utils.ViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rolangom on 11/7/14.
 */
public class ServiceListAdapter extends BaseAdapter {

    List<ServiceItem> mList;
    LayoutInflater mInflater;

    public ServiceListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        mList = new ArrayList<ServiceItem>();
    }

    public void addRange(List<ServiceItem> newRange) {
        mList.addAll(newRange);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public ServiceItem getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null)
            view = mInflater.inflate(R.layout.item_list_service, viewGroup, false);

        View lineView = ViewHolder.get(view, R.id.vline);
        TextView titleView = ViewHolder.get(view, R.id.title);
        TextView addressView = ViewHolder.get(view, R.id.descr);

        ServiceItem item = getItem(i);

        lineView.setBackgroundResource(item.getServiceType().getColorRes());
        titleView.setTextColor(item.getServiceType().getColorRes());
        titleView.setText(item.getServiceType().getStringRes());
        addressView.setText(item.getAddress());

        return view;
    }
}
