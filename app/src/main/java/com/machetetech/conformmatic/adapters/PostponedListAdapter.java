package com.machetetech.conformmatic.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.machetetech.conformmatic.R;
import com.machetetech.conformmatic.models.PostponeItem;
import com.machetetech.conformmatic.utils.Funcs;
import com.machetetech.conformmatic.utils.ViewHolder;

import java.util.List;

/**
 * Created by rolangom on 1/28/15.
 */
public class PostponedListAdapter extends BaseAdapter {

    private final List<PostponeItem> mList;
    private final LayoutInflater mInflater;

    public PostponedListAdapter(Context context, List<PostponeItem> list) {
        mInflater = LayoutInflater.from(context);
        mList = list;
    }

    public void addAll(List<PostponeItem> newItems) {
        mList.addAll(newItems);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public PostponeItem getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = mInflater.inflate(R.layout.item_list_postponed, parent, false);
        TextView messageView = ViewHolder.get(convertView, android.R.id.message);
        TextView dateView = ViewHolder.get(convertView, android.R.id.text1);
        TextView userView = ViewHolder.get(convertView, android.R.id.text2);

        PostponeItem item = getItem(position);

        messageView.setText(item.reason);
        dateView.setText(Funcs.getString(item.date));
        userView.setText(item.user);

        return convertView;
    }
}
