package com.machetetech.conformmatic.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.machetetech.conformmatic.R;
import com.machetetech.conformmatic.models.PictureItem;
import com.machetetech.conformmatic.utils.ViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rolangom on 11/11/14.
 */
public class PictureListAdapter extends BaseAdapter {

    List<PictureItem> mList;
    LayoutInflater mInflater;

    public PictureListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        mList = new ArrayList<PictureItem>();
    }

    public void add(PictureItem item) {
        mList.add(item);
        notifyDataSetChanged();
    }

    void delete(int i) {
        mList.remove(i);
        notifyDataSetChanged();
    }

    public List<Uri> getUriList() {
        List<Uri> list = new ArrayList<>();
        for (PictureItem item : mList) {
            list.add(item.getUri());
        }
        return list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public PictureItem getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            view = mInflater.inflate(R.layout.item_list_picture, viewGroup, false);
            setOnClickListener (view, viewGroup);
        }
        PictureItem item = getItem(i);

        ImageView imageView = ViewHolder.get(view, android.R.id.icon);
        imageView.setImageURI(item.getUri());

        return view;
    }

    void setOnClickListener(final View view, final ViewGroup viewGroup) {
        view.findViewById(android.R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete(view, viewGroup);
            }
        });
    }

    void delete(View view, ViewGroup viewGroup) {
        ListView listView = (ListView)viewGroup;
        int pos = listView.getPositionForView(view);
        delete(pos);
    }
}
