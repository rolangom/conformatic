package com.machetetech.conformmatic.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.location.Location;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.machetetech.conformmatic.models.ServiceClient;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.util.Arrays;

/**
 * Created by rolangom on 12/10/14.
 */
public class HttpClientUtils {

    private static final AsyncHttpClient sClient = new AsyncHttpClient();

    public static void login (Context context, String user, String password, AsyncHttpResponseHandler responseHandler)
            throws Exception {
        try {
            JSONObject body = new JSONObject();
            body.put("Usuario", user);
            body.put("Clave", password);
            Header header = new BasicHeader("Content-Type", Consts.JSON_CONTENT_TYPE);
            StringEntity entity = new StringEntity(body.toString());
            sClient.post(context, Consts.BASE_URL + "Tecnico/login", new Header[] { header }, entity, Consts.JSON_CONTENT_TYPE, responseHandler);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void getServices (Context context, AsyncHttpResponseHandler responseHandler)
            throws Exception {
        Log.i(context.getClass().getName(), Consts.BASE_URL + "Tecnico/Servicios");
        sClient.post(Consts.BASE_URL + "Tecnico/Servicios", responseHandler);
    }

    public static void postpone(Context context, ServiceClient item, AsyncHttpResponseHandler responseHandler)
     throws Exception {
        try {
            JSONObject body = new JSONObject();
            body.put("Servicio_Secuencia", item.servSeq);
            body.put("Servicio_Pospuesto_Razon", item.servReason);
            Header header = new BasicHeader("Content-Type", Consts.JSON_CONTENT_TYPE);
            StringEntity entity = new StringEntity(body.toString());
            sClient.post(context, Consts.BASE_URL + "Tecnico/Servicio/Posponer", new Header[] { header }, entity, Consts.JSON_CONTENT_TYPE, responseHandler);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void getPostponedList(int serviceId, AsyncHttpResponseHandler responseHandler) {
        sClient.get(String.format("%sTecnico/Servicio/PospuestoRazones?servicioId=%d",
                Consts.BASE_URL, serviceId), responseHandler);
    }

    public static void update(Context context, ServiceClient item, AsyncHttpResponseHandler responseHandler) {
        try {
            Header header = new BasicHeader("Content-Type", Consts.JSON_CONTENT_TYPE);
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZ").create();
            String jsonStr = gson.toJson(item);
            Log.d(context.getClass().getName(), "Update Servicio: "+jsonStr);
            StringEntity entity = new StringEntity(jsonStr);
            sClient.post(context, Consts.BASE_URL + "Tecnico/ActualizarServicios", new Header[] { header },
                    entity, Consts.JSON_CONTENT_TYPE, responseHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static void upload(Context context, Uri fileUri, int serviceId, AsyncHttpResponseHandler responseHandler)
            throws FileNotFoundException {
        RequestParams params = new RequestParams();
        ContentResolver cR = context.getContentResolver();
        Log.d(context.getClass().getName(), "Uploading ServicioId "+serviceId +" file: " + fileUri.getPath()+ " media type: " + cR.getType(fileUri));
        params.put(fileUri.getLastPathSegment(), new File(fileUri.getPath()));
        sClient.post(Consts.BASE_URL + "Tecnico/Servicio/SubirFotos?servicioId=" + serviceId, params, responseHandler);
    }

    public static void location(Context context, Location location, AsyncHttpResponseHandler responseHandler) throws JSONException, UnsupportedEncodingException {
        JSONObject body = new JSONObject();
        body.put("Lactitud", Double.toString(location.getLatitude()));
        body.put("Longitud", Double.toString(location.getLongitude()));
        Log.d(context.getClass().getName(), "location body: "+body.toString());
        Header header = new BasicHeader("Content-Type", Consts.JSON_CONTENT_TYPE);
        StringEntity entity = new StringEntity(body.toString());
        sClient.post(context, Consts.BASE_URL + "Tecnico/Localizacion", new Header[] { header }, entity, Consts.JSON_CONTENT_TYPE, responseHandler);
    }

    public static void sendRegId(String regId, AsyncHttpResponseHandler responseHandler) {
        String url = String.format("%sTecnico/Servicio/TecnicoToken?&token=%s", Consts.BASE_URL, regId);
        Log.d("sendRegId -> ", url);
        sClient.post(url, responseHandler);
    }

    public static void setToken(String token) {
        sClient.addHeader("Authorization", "Bearer " + token);
    }

    public static void removeToken() {
        sClient.removeHeader("Authorization");
    }

}
