package com.machetetech.conformmatic.utils;

/**
 * Created by rolangom on 11/11/14.
 */
public class Consts {

    public static final int REQUEST_IMAGE_CAPTURE = 1;
    public static final int REQUEST_IMAGE_GALLERY = 2;
    public static final String BASE_URL = "http://54.187.93.4/RandyApi/api/";
    public static final String JSON_CONTENT_TYPE = "application/json";
    public static final String ACCESS_TOKEN = "AccessToken";

    public static final int SERVICE_STATUS_PENDING = 1;
    public static final int SERVICE_STATUS_ASSIGNED = 2;
    public static final int SERVICE_STATUS_CONCLUDED = 3;
    public static final int SERVICE_STATUS_POSTPONED = 4;

    private final static int
            CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    // SHARED PREFERENCES
    public static final String GCM_TOKEN = "gcm_token";

    // Push Notification
    public static final String DATA_PUSHED = "data_pushed";
    public static final String REG_ID_PREF = "reg_id";

    public static final String PROJECT_NUMBER = "1073183999272";
}
