package com.machetetech.conformmatic.utils;

import android.content.Context;
import android.content.res.Resources;

import com.machetetech.conformmatic.R;
import com.machetetech.conformmatic.enums.ServiceType;
import com.machetetech.conformmatic.models.ServiceItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by rolangom on 11/7/14.
 */
public class RandomUtils {

    Context mContext;
    Resources mResources;
    Random mRandom;

    String[] mNotes;
    String[] mAddresses;

    final int FEED_SIZE = 20;
    int mPage;

    public RandomUtils(Context context) {
        mContext = context;
        mResources = context.getResources();
        mRandom = new Random();

        mAddresses = mResources.getStringArray(R.array.addresses);
        mNotes = mResources.getStringArray(R.array.notes);
        mPage = 0;
    }

    public ServiceType getServiceType() {
        return ServiceType.values()[mRandom.nextInt(ServiceType.values().length)];
    }

    public String getAddress() {
        return mAddresses[mRandom.nextInt(mAddresses.length)];
    }

    public String getNote() {
        return mNotes[mRandom.nextInt(mNotes.length)];
    }

    public List<ServiceItem> getServiceItems() {
        mPage++;
        List<ServiceItem> list = new ArrayList<ServiceItem>();
        for (int i = 0; i < FEED_SIZE; i++) {
            ServiceItem item = new ServiceItem(getServiceType(), getAddress(), getNote());
            list.add(item);
        }
        return list;
    }
}
