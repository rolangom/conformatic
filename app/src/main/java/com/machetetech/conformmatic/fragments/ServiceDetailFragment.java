package com.machetetech.conformmatic.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.internal.mv;
import com.machetetech.conformmatic.R;
import com.machetetech.conformmatic.activities.MainActivity;
import com.machetetech.conformmatic.models.ServiceClient;
import com.machetetech.conformmatic.models.ServiceItem;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceDetailFragment extends Fragment {

    ServiceClient mServiceItem;

    View mLineView;
    TextView mTitleView;
    TextView mAddressView;
    TextView mNotesView;

    EditText mSerialsView, mInvoiceIdView;

    Button mPostponeBtn, mSaveBtn;
    MainActivity mActivity;

    public ServiceDetailFragment() { }

    public void setServiceItem(ServiceClient serviceItem) {
        mServiceItem = serviceItem;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MainActivity)activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_service_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi(view);
        initData();
    }

    void initUi(View view) {
        mLineView = view.findViewById(R.id.vline);
        mTitleView = (TextView) view.findViewById(R.id.title);
        mAddressView = (TextView) view.findViewById(R.id.address);
        mNotesView = (TextView) view.findViewById(R.id.notes);

        mSerialsView = (EditText) view.findViewById(R.id.serials);
        mInvoiceIdView = (EditText) view.findViewById(R.id.invoice_id);

        mPostponeBtn = (Button) view.findViewById(R.id.postpone_btn);
        mSaveBtn = (Button) view.findViewById(R.id.save_btn);

        mPostponeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPostpone();
            }
        });
        mSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save();
            }
        });
    }

    void initData() {
        mLineView.setBackgroundResource(mServiceItem.getColorRes());
        mTitleView.setText(mServiceItem.servType);
        mAddressView.setText(mServiceItem.address);
        mNotesView.setText(mServiceItem.notes);
    }

    void showPostpone() {
        PostponeFragment fragment = new PostponeFragment();
        fragment.setServiceItem(mServiceItem);
        mActivity.removeFragment(this);
        mActivity.showFragment(fragment);
    }

    void save() {
        mServiceItem.serials = mSerialsView.getText().toString();
        mServiceItem.billId = mInvoiceIdView.getText().toString();
        PictureListFragment fragment = new PictureListFragment();
        fragment.setServiceClient(mServiceItem);
        mActivity.showFragment(fragment);
    }
}
