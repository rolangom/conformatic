package com.machetetech.conformmatic.fragments;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;
import com.machetetech.conformmatic.R;
import com.machetetech.conformmatic.activities.MainActivity;
import com.machetetech.conformmatic.adapters.ServiceClientAdapter;
import com.machetetech.conformmatic.models.ServiceClient;
import com.machetetech.conformmatic.utils.HttpClientUtils;

import org.apache.http.Header;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rolangom on 11/7/14.
 */
public class ServicesFragment extends ListFragment {

    View mProgressBar;
    TextView mNoDataView;

    ServiceClientAdapter mAdapter;
    boolean mIsLoading;

    MainActivity mActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_services, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi(view);
        initData();
    }

    void initUi(View view) {
        mProgressBar = view.findViewById(android.R.id.progress);
        mNoDataView = (TextView)view.findViewById(android.R.id.empty);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        int pos = l.getPositionForView(v);
        ServiceClient item = mAdapter.getItem(pos);
        showFragmentDetail(item);
    }

    void showFragmentDetail(ServiceClient item) {
        ServiceDetailFragment fragment = new ServiceDetailFragment();
        fragment.setServiceItem(item);
        mActivity.showFragment(fragment);
    }

    void initData() {
        initAdapter();
        mNoDataView.setVisibility(View.GONE);
        try {
            mIsLoading = true;
            HttpClientUtils.getServices(mActivity, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.e(this.getClass().getName(), responseString, throwable);
                    mIsLoading = false;
                    showError(throwable.getMessage());
                    if (statusCode == 401) // Unauthorized, so logout
                        mActivity.logout();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Type collectionType = new TypeToken<List<ServiceClient>>() { }.getType();
                    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
                    List<ServiceClient> items = gson.fromJson(responseString, collectionType);
                    mIsLoading = false;
                    handleDataResult(items);
                }
            });
        } catch (Exception e) {
            mIsLoading = false;
            e.printStackTrace();
            showError(e.getMessage());
        }
    }

    void showError(String message) {
        mActivity.showErrorDialog(message);
        setProgressVisible(false);
        mNoDataView.setText(R.string.error_loading_data);
        mNoDataView.setVisibility(View.VISIBLE);
    }

    void handleDataResult(List<ServiceClient> items) {
        List<ServiceClient> resultList = new ArrayList<ServiceClient>();
        for (ServiceClient item : items)
            if (item.isVisible())
                resultList.add(item);

        mAdapter.addRange(resultList);
        setProgressVisible(false);

        if (mAdapter.getCount() == 0)
            mNoDataView.setVisibility(View.VISIBLE);
    }

    void setProgressVisible(boolean isVisible) {
        mProgressBar.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    void initAdapter() {
        mAdapter = new ServiceClientAdapter(mActivity);
        setListAdapter(mAdapter);
    }

    public void reload() {
        if (!mIsLoading) {
            setProgressVisible(true);
            initData();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MainActivity)activity;
    }
}
