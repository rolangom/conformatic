package com.machetetech.conformmatic.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.loopj.android.http.TextHttpResponseHandler;
import com.machetetech.conformmatic.R;
import com.machetetech.conformmatic.activities.MainActivity;
import com.machetetech.conformmatic.models.ServiceClient;
import com.machetetech.conformmatic.utils.HttpClientUtils;
import com.machetetech.conformmatic.utils.UiCommonUtils;
import com.machetetech.conformmatic.views.SignatureView;

import org.apache.http.Header;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignatureFragment extends Fragment implements View.OnClickListener {

    MainActivity mActivity;
    SignatureView mSignatureView;
    Button mClearBtn, mFinishBtn;
    View mViewClicked;

    AlertDialog mConfirmDialog;
    Uri mFileUri;

    ServiceClient mServiceClient;

    int mPicIndex;

    public SignatureFragment() {
        // Required empty public constructor
    }

    public void setServiceClient(ServiceClient serviceClient) {
        mServiceClient = serviceClient;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MainActivity)getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_signature, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi(view);
    }

    void initUi (View view) {
        mClearBtn = (Button)view.findViewById(R.id.clear);
        mFinishBtn = (Button)view.findViewById(R.id.save_btn);
        mSignatureView = (SignatureView)view.findViewById(R.id.signature);

        mClearBtn.setOnClickListener(this);
        mFinishBtn.setOnClickListener(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setMessage(R.string.confirm_finish)
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (mViewClicked.getId() == R.id.save_btn)
                            finish();
                        else
                            mSignatureView.clear();
                    }
                });

        mConfirmDialog = builder.create();
    }

    @Override
    public void onClick(View view) {
        mViewClicked = view;
        showDialog (mViewClicked.getId() == R.id.clear ?
                R.string.confirm_clear : R.string.confirm_finish);
    }

    void showDialog (int stringId) {
        mConfirmDialog.setMessage(getString(stringId));
        mConfirmDialog.show();
    }

    void saveSignatureImage () {
        try {
            mSignatureView.setDrawingCacheEnabled(true);
            Bitmap bitmap = mSignatureView.getDrawingCache();
            mFileUri = UiCommonUtils.getOutputMediaFileUri(UiCommonUtils.MEDIA_TYPE_IMAGE);
            File f = new File(mFileUri.getPath());

            Toast.makeText(mActivity, "Image saved to:\n" +
                    mFileUri.getPath(), Toast.LENGTH_LONG).show();

            FileOutputStream ostream = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 10, ostream);
            ostream.close();

            galleryAddPic();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void close () {
        mActivity.dissmissProgressDialog();
        mActivity.finishFragmentFlow();
    }

    void finish () {
        mActivity.showProgressDialog();
        saveSignatureImage();
        if (mFileUri != null)
            mServiceClient.imageUris.add(mFileUri);
        mServiceClient.setAsConcluded();
        mServiceClient.finishDate = new Date();
        mPicIndex = mServiceClient.imageUris.size();
        mActivity.sendLocation();
        updateService();
        mActivity.setProgressDialogMessage(getString(R.string.uploading_images));
        uploadImages();
    }

    void updateService() {
        HttpClientUtils.update(mActivity, mServiceClient, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                mServiceClient.setAsPostponed();
                mActivity.dissmissProgressDialog();
                Log.e(this.getClass().getName(), responseString, throwable);
                mActivity.showErrorDialog(responseString);
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                mActivity.dissmissProgressDialog();
                Log.d(this.getClass().getName(), responseString);
                uploadImages();
            }
        });
    }

    void uploadImages() {
        Log.i(this.getClass().getName()+"->uploadImages", "mPicIndex: " + mPicIndex);
        if (mPicIndex > 0) {
            mPicIndex --;
            Uri uri = mServiceClient.imageUris.get(mPicIndex);
            try {
                Log.i(this.getClass().getName(), "uploading img " + (mPicIndex) + ", uri: " + uri.getPath());
                HttpClientUtils.upload(mActivity, uri, mServiceClient.servSeq, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Log.e(this.getClass().getName(), responseString, throwable);
                        uploadImages();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        Log.d(this.getClass().getName(), responseString);
                        uploadImages();
                    }
                });
            } catch (FileNotFoundException e) {
                Log.e(this.getClass().getName(), "file not found " + uri.getPath());
                e.printStackTrace();
                uploadImages();
            }
        } else
            close();
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(mFileUri);
        mActivity.sendBroadcast(mediaScanIntent);
    }
}
