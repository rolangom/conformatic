package com.machetetech.conformmatic.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.machetetech.conformmatic.R;
import com.machetetech.conformmatic.activities.MainActivity;
import com.machetetech.conformmatic.adapters.PictureListAdapter;
import com.machetetech.conformmatic.models.PictureItem;
import com.machetetech.conformmatic.models.ServiceClient;
import com.machetetech.conformmatic.utils.Consts;
import com.machetetech.conformmatic.utils.UiCommonUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class PictureListFragment extends Fragment {

    MainActivity mActivity;

    ListView mListView;
    View mContinueBtn, mCameraBtn;

    PictureListAdapter mAdapter;

    AlertDialog mOptionDialog;

    ServiceClient mServiceClient;

    private Uri mFileUri;

    public PictureListFragment() {
        // Required empty public constructor
        setRetainInstance(true);
    }

    public void setServiceClient(ServiceClient serviceClient) {
        mServiceClient = serviceClient;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MainActivity)activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pictures, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi(view);
    }

    void initUi(View view) {
        mListView = (ListView) view.findViewById(android.R.id.list);
        mAdapter = new PictureListAdapter(mActivity);
        mListView.setAdapter(mAdapter);

        mContinueBtn = view.findViewById(android.R.id.button1);
        mCameraBtn = view.findViewById(android.R.id.button2);

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity)
                .setTitle(R.string.choose)
                .setItems(R.array.get_image_options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == 0)
                            launchCamera();
                        else
                            launchGallery();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // auto dismissed;
                        dialogInterface.dismiss();
                    }
                });
        mOptionDialog = builder.create();

        mContinueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onContinue();
            }
        });
        mCameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOptionDialog.show();
            }
        });
    }

    void onContinue() {
        if (mAdapter.getCount() > 0)
            mServiceClient.imageUris.addAll(mAdapter.getUriList());
        SignatureFragment fragment = new SignatureFragment();
        fragment.setServiceClient(mServiceClient);
        mActivity.showFragment(fragment);
    }

    void launchCamera() {
        // create Intent to take a picture and return control to the calling application
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        mFileUri = UiCommonUtils.getOutputMediaFileUri(UiCommonUtils.MEDIA_TYPE_IMAGE); // create a file to save the image
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mFileUri); // set the image file name

        // start the image capture Intent
        startActivityForResult(intent, Consts.REQUEST_IMAGE_CAPTURE);
    }

    void launchGallery() {
        Intent i = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, Consts.REQUEST_IMAGE_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Consts.REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            // Image captured and saved to fileUri specified in the Intent
            Toast.makeText(mActivity, "Image saved to:\n" +
                    mFileUri, Toast.LENGTH_LONG).show();

            addedImage();
            galleryAddPic();

        } else if (requestCode == Consts.REQUEST_IMAGE_GALLERY && resultCode == Activity.RESULT_OK) {
            // Image captured and saved to fileUri specified in the Intent
            Toast.makeText(mActivity, "Image saved to:\n" +
                    data.getData(), Toast.LENGTH_LONG).show();

            mFileUri = getRealPathFromUri(data.getData());
            addedImage();
        }
    }

    void addedImage() {
        mAdapter.add(new PictureItem(mFileUri));
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(mFileUri);
        mActivity.sendBroadcast(mediaScanIntent);
    }

    public Uri getRealPathFromUri(Uri contentUri) {
        String res = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = mActivity.getContentResolver().query(contentUri, proj, null, null, null);
        if(cursor.moveToFirst()){;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return Uri.parse(res);
    }
}
