package com.machetetech.conformmatic.fragments;


import android.app.ListFragment;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;
import com.machetetech.conformmatic.R;
import com.machetetech.conformmatic.adapters.PostponedListAdapter;
import com.machetetech.conformmatic.models.PostponeItem;
import com.machetetech.conformmatic.utils.HttpClientUtils;

import org.apache.http.Header;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostponedListFragment extends ListFragment {

    int mServiceItemId;

    public static PostponedListFragment newInstance(int serviceItemId) {
        PostponedListFragment frag = new PostponedListFragment();
        Bundle args = new Bundle();
        args.putInt("ServiceItemId", serviceItemId);
        frag.setArguments(args);
        return frag;
    }

    public PostponedListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null && args.containsKey("ServiceItemId"))
            mServiceItemId = args.getInt("ServiceItemId");

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListAdapter(new PostponedListAdapter(getActivity(), new ArrayList<PostponeItem>()));
        setListShown(false);
        initData();
    }

    void initData() {
        try {
            HttpClientUtils.getPostponedList(mServiceItemId, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.e(this.getClass().getSimpleName(), responseString, throwable);
                    setListShown(true);
                    setEmptyText(getString(R.string.error_loading_data));
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Type collectionType = new TypeToken<List<PostponeItem>>() { }.getType();
                    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
                    List<PostponeItem> items = gson.fromJson(responseString, collectionType);
                    handleDataResult(items);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            setListShown(true);
            setEmptyText(getString(R.string.error_loading_data));
        }
    }

    private void handleDataResult(List<PostponeItem> items) {
        if (items.size() > 0) {
            ((PostponedListAdapter) getListAdapter()).addAll(items);
            setListShown(true);
        } else
            setEmptyText(getString(R.string.no_data));
    }
}
