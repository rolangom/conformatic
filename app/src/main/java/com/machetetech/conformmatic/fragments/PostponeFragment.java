package com.machetetech.conformmatic.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.TextHttpResponseHandler;
import com.machetetech.conformmatic.R;
import com.machetetech.conformmatic.activities.MainActivity;
import com.machetetech.conformmatic.models.ServiceClient;
import com.machetetech.conformmatic.utils.HttpClientUtils;

import org.apache.http.Header;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostponeFragment extends Fragment {

    ServiceClient mServiceItem;

    View mSaveBtn;
    View mSeeHistorialBtn;
    EditText mTextView;

    MainActivity mActivity;

    public PostponeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MainActivity)activity;
    }

    public void setServiceItem(ServiceClient serviceItem) {
        mServiceItem = serviceItem;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_postpone, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    void init(View view) {
        mTextView = (EditText) view.findViewById(R.id.postpone_view);
        mSaveBtn = view.findViewById(R.id.save_btn);
        mSeeHistorialBtn = view.findViewById(android.R.id.button1);

        mSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save();
            }
        });
        mSeeHistorialBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seeHistorial();
            }
        });
    }

    private void seeHistorial() {
        mActivity.showFragment(PostponedListFragment.newInstance(mServiceItem.servSeq));
    }

    void save() {
        if (mTextView.getText().length() == 0) {
            mTextView.setError(getString(R.string.error_field_required));
            return;
        }
        mServiceItem.setAsPostponed();
        mServiceItem.servReason = mTextView.getText().toString();

        mActivity.showProgressDialog();

        try {
            HttpClientUtils.postpone(mActivity, mServiceItem, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d(this.getClass().getName(), responseString);
                    Toast.makeText(mActivity, responseString, Toast.LENGTH_LONG).show();
                    mActivity.dissmissProgressDialog();
                    mActivity.showErrorDialog(responseString);
                }
                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Log.d(this.getClass().getName(), responseString);
                    mActivity.dissmissProgressDialog();
                    mActivity.removeFragment(PostponeFragment.this);
                }
            });
        }catch (Exception e) {
            e.printStackTrace();
            mActivity.dissmissProgressDialog();
            mActivity.showErrorDialog(e.getMessage());
        }
    }

}
