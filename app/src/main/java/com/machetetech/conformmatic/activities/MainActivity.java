package com.machetetech.conformmatic.activities;

import android.app.Activity;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.loopj.android.http.TextHttpResponseHandler;
import com.machetetech.conformmatic.R;
import com.machetetech.conformmatic.fragments.NavigationDrawerFragment;
import com.machetetech.conformmatic.fragments.ServicesFragment;
import com.machetetech.conformmatic.utils.Consts;
import com.machetetech.conformmatic.utils.HttpClientUtils;

import org.apache.http.Header;
import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class MainActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {

    ServicesFragment mServicesFragment;
    ProgressDialog mProgressDialog;
    Location mLastLocation;
    GoogleApiClient mGoogleApiClient;
    GoogleCloudMessaging mGcm;

    SharedPreferences mPrefs;
    SharedPreferences.Editor mEditor;

    String sGcmRegId;
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;


    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActionBar().setDisplayUseLogoEnabled(true);
        getActionBar().setDisplayShowTitleEnabled(false);

        mServicesFragment = new ServicesFragment();

        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        initGoogleApi();

        mPrefs = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        mEditor = mPrefs.edit();
        getRegId();
    }

    void initGoogleApi() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build();
    }

    public void showErrorDialog(String error) {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }

    public void showProgressDialog() {
        mProgressDialog = ProgressDialog.show(
              this
            , null // No Title
            , getString(R.string.please_wait)
            , true
            , false);
    }

    public void setProgressDialogMessage(String msg) {
        if (mProgressDialog != null)
            mProgressDialog.setMessage(msg);
    }

    public void dissmissProgressDialog() {
        mProgressDialog.dismiss();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Fragment resultFragment = null;
        switch (position) {
            case 0:
                resultFragment = mServicesFragment;
                break;
            case 1:
                handleLogout();
                return;
            default:
                //TODO
                break;
        }
        onSectionAttached(position);
        finishFragmentFlow();
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, resultFragment)
                .commit();
    }

    void handleLogout() {
        new AlertDialog.Builder(this)
            .setMessage(getString(R.string.confirm_logout))
            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    logout();
                }
            })
            .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            })
            .create()
            .show();
    }

    public void logout() {
        SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE).edit();
        editor.remove(Consts.ACCESS_TOKEN);
        editor.apply();
        HttpClientUtils.removeToken();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 0:
                mTitle = getString(R.string.services);
            case 1:
//                mTitle = getString(R.string.log_out);
                break;
            case 2:
                break;
        }
    }

    public void showFragment(Fragment fragment) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack("flow")
                .commit();
    }

    public void removeFragment(Fragment fragment) {
        FragmentManager manager = getFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        trans.remove(fragment);
        trans.commit();
        manager.popBackStack();
    }

    public void finishFragmentFlow () {
        FragmentManager manager = getFragmentManager();
        for (int i =0; i < manager.getBackStackEntryCount(); i++)
            manager.popBackStack();
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.refresh) {
            mServicesFragment.reload();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void requestLocationUpdate() {
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
            return;
        }
        LocationRequest request = new LocationRequest();
        request.setNumUpdates(1);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, request, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//        if (mLastLocation == null)
        requestLocationUpdate();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        sendLocation();
    }

    public void sendLocation() {
        if (mLastLocation != null) {
            Log.d(this.getClass().getName(), "lat: "+mLastLocation.getLatitude()+", lng: "+mLastLocation.getLongitude());
            try {
                HttpClientUtils.location(this, mLastLocation, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Log.e(this.getClass().getName(), responseString, throwable);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        Log.d(this.getClass().getName(), responseString);
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    public void getRegId(){
        if (mPrefs.contains(Consts.REG_ID_PREF)){
            sGcmRegId = mPrefs.getString(Consts.REG_ID_PREF, "");
            registerInBackend();
        }else // if we don't have the reg ID
            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    String msg = "";
                    try {
                        if (mGcm == null) {
                            mGcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                        }
                        sGcmRegId = mGcm.register(Consts.PROJECT_NUMBER);
                        msg = "Device registered, registration ID=" + sGcmRegId;
                        Log.i("GCM", msg);

                        mEditor.putString("regId", sGcmRegId);
                        mEditor.commit();
                    } catch (IOException ex) {
                        msg = "Error :" + ex.getMessage();
                        ex.printStackTrace();
                    }
                    return msg;
                }

                @Override
                protected void onPostExecute(String msg) {
                    Log.i("GCM",  msg);
                    registerInBackend();
                }
            }.execute(null, null, null);
    }

    void registerInBackend() {
       HttpClientUtils.sendRegId(sGcmRegId, new TextHttpResponseHandler() {
           @Override
           public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
               Log.d(this.getClass().getSimpleName(), String.format("registerInBackend statusCode: %d, responseString: %s", statusCode, responseString));
           }

           @Override
           public void onSuccess(int statusCode, Header[] headers, String responseString) {
               Log.d(this.getClass().getSimpleName(), String.format("registerInBackend statusCode: %d, responseString: %s", statusCode, responseString));
           }
       });
    }
}
