package com.machetetech.conformmatic.enums;

/**
 * Created by rolangom on 11/8/14.
 */
public enum ServiceStatus {
    PENDING,
    POSTPONED,
    COMPLETED
}
