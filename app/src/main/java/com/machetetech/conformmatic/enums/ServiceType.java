package com.machetetech.conformmatic.enums;

import com.machetetech.conformmatic.R;

/**
 * Created by rolangom on 11/7/14.
 */
public enum ServiceType {
    INSTALLATION(R.color.blue, R.string.service_installation),
    DELIVERY(R.color.green, R.string.service_delivery),
    WARRANTY(R.color.orange, R.string.service_warranty);

    int colorRes;
    int stringRes;

    ServiceType(int colorRes, int stringRes) {
        this.colorRes = colorRes;
        this.stringRes = stringRes;
    }

    public int getColorRes() {
        return colorRes;
    }

    public int getStringRes() {
        return stringRes;
    }
}
