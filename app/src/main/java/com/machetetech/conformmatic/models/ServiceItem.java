package com.machetetech.conformmatic.models;

import com.machetetech.conformmatic.enums.ServiceStatus;
import com.machetetech.conformmatic.enums.ServiceType;

/**
 * Created by rolangom on 11/7/14.
 */
public class ServiceItem {

    Long id;

    ServiceType serviceType;
    ServiceStatus serviceStatus;

    String address;
    String serials;
    String invoiceId;
    String notes;

    String postponedReason;

    public ServiceItem(ServiceType serviceType, String address, String notes) {
        this.serviceType = serviceType;
        this.address = address;
        this.notes = notes;
        serviceStatus = ServiceStatus.PENDING;
    }

    public void setServiceStatus(ServiceStatus serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public void setPostponedReason(String postponedReason) {
        this.postponedReason = postponedReason;
    }

    public void setSerials(String serials) {
        this.serials = serials;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getPostponedReason() {
        return postponedReason;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    public String getAddress() {
        return address;
    }

    public String getSerials() {
        return serials;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public String getNotes() {
        return notes;
    }
}
