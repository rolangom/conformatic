package com.machetetech.conformmatic.models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by rolangom on 1/28/15.
 */
public class PostponeItem {

    @SerializedName("Usuario") public String user;
    @SerializedName("Razon") public String reason;
    @SerializedName("RazonId") public int reasonId;
    @SerializedName("ServicioId") public int serviceId;
    @SerializedName("Fecha") public Date date;

}
