package com.machetetech.conformmatic.models;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by rolangom on 11/12/14.
 */
public class PictureItem {

    Bitmap bitmap;
    String imagePath;
    Uri uri;

    public PictureItem(Uri uri) {
        this.uri = uri;
    }

    public PictureItem(Bitmap bitmap, String imagePath) {
        this.bitmap = bitmap;
        this.imagePath = imagePath;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public String getImagePath() {
        return imagePath;
    }

    public Uri getUri() {
        return uri;
    }
}
