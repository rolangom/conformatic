package com.machetetech.conformmatic.models;

import android.net.Uri;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.machetetech.conformmatic.R;
import com.machetetech.conformmatic.utils.Consts;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by rolangom on 12/24/14.
 */
public class ServiceClient {

    @SerializedName("Cliente_Codigo") public String code;
    @SerializedName("Cliente_Datos") public String data;
    @SerializedName("Cliente_Nombres") public String firstName;
    @SerializedName("Cliente_Apellidos") public String lastName;
    @SerializedName("Cliente_Secuencia") public String seq;

    @SerializedName("Registro_Estado") public String statusReg;
    @SerializedName("Registro_Fecha") public Date regDate;
    @SerializedName("Registro_Usuario") public String userReg;

    @SerializedName("Servicio_Contacto") public String contact;
    @SerializedName("Servicio_Direccion") public String address;
    @SerializedName("Servicio_Estado_Explicacion") public String statusDescr;
    @SerializedName("Servicio_Estado_Nombre") public String status;
    /**
     *
     * 0: {Servicio_Estado_Secuencia: 1, Servicio_Estado_Nombre: "Pendiente",…}
     * 1: {Servicio_Estado_Secuencia: 4, Servicio_Estado_Nombre: "Pospuesto",…}
     * 2: {Servicio_Estado_Secuencia: 2, Servicio_Estado_Nombre: "Asignado",…}
     * 3: {Servicio_Estado_Secuencia: 3, Servicio_Estado_Nombre: "Concluido ",…}
     */
    @Expose @SerializedName("Servicio_Estado_Secuencia") public int statusSeq;
    @Expose @SerializedName("Servicio_Factura") public String billId;
    @SerializedName("Servicio_Fecha") public Date servDate;
    @Expose @SerializedName("Servicio_Fecha_Terminado") public Date finishDate;
    @SerializedName("Servicio_Motivo") public String servReason;
    @SerializedName("Servicio_Recibo") public String servReceipt;
    @Expose @SerializedName("Servicio_Secuencia") public int servSeq;
    @Expose @SerializedName("Servicio_Equipos_Seriales") public String serials;
    @Expose @SerializedName("Servicio_Observaciones") public String notes;

    /**
     * 0: {Servicio_Tipo_Secuencia: 2, Servicio_Tipo_Nombre: "Chequeo de Aire",…}
     * 1: {Servicio_Tipo_Secuencia: 1, Servicio_Tipo_Nombre: "Instalación",…}
     */
    @SerializedName("Servicio_Tipo_Secuencia") public int servTypeSeq;
    @SerializedName("Servicio_Tipo_Nombre") public String servType;
    @SerializedName("Servicio_Tipo_Explicacion") public String servTypeDesr;

    public transient List<Uri> imageUris = new ArrayList<>();

    public int getColorRes() {
        switch (servTypeSeq) {
            case 1:
                return R.color.blue;
            case 2:
                return R.color.orange;
            case 3:
                return R.color.green;
            default:
                return R.color.gray_darkest;
        }
    }

    public boolean isPostponed() {
        return statusSeq == Consts.SERVICE_STATUS_POSTPONED;
    }

    public boolean isVisible() {
        return statusSeq == Consts.SERVICE_STATUS_ASSIGNED || statusSeq == Consts.SERVICE_STATUS_POSTPONED;
    }

    public void setAsPostponed() {
        statusSeq = Consts.SERVICE_STATUS_POSTPONED;
    }

    public void setAsConcluded() {
        statusSeq = Consts.SERVICE_STATUS_CONCLUDED;
    }

    public ServiceClient() {

    }
}
